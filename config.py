import logging

import pygame as pg

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(message)s')
logger = logging.getLogger()

# -------------------------------WINDOW/COORD SETTINGS--------------------------
window_title = '10 iq Tetris'
field_size = 40
x_count = 10
y_count = 23
padding = (field_size, field_size // 2)
framerate = 100

width = field_size * x_count + padding[0] * 2
height = round(field_size * (y_count + 1.5)) + padding[1] * 2 + 40


# --------------------------------COLOR SETTINGS--------------------------------
background_color = pg.Color('navajowhite')


# --------------------------------FONT SETTINGS---------------------------------
font_type = 'consolas'
font_size = int(field_size / 2.1)
house_font_size = int(field_size / 2.3)
win_font_size = int(field_size * 2)
