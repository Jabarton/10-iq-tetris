import random
from threading import Thread
from typing import List, Tuple

import numpy as np

import config as cfg


class Board:
    def __init__(self):
        self.raw_board = []
        self.fields = {}

        for i in range(cfg.y_count):
            self.raw_board.append([])
            for j in range(cfg.x_count):
                self.raw_board[i].append(0)
                self.fields[(j, i)] = Field()


class Field:
    def __init__(self, occupied: bool = False, color='black'):
        self.occupied = occupied
        self.color = color

    def __str__(self):
        return f'occupied: {self.occupied}, color: {self.color}'


class Game(Thread):
    def __init__(self):
        super().__init__(daemon=True)
        self.board = Board()
        self.gravity = 2
        self.piece = None

    def spawn_new_piece(self):
        print('spawning new piece')
        self.piece = random.choice(ALL_PIECES)()

    def gravity_piece(self):
        if self.gravity > 0:
            self.move_piece_rel(0, 1)

    def is_piece_stopped(self) -> bool:
        if self.piece.reached_bottom():
            return True

        for j, row in enumerate(self.piece.body):
            for i, cell in enumerate(row):
                if cell and self.board.fields[(self.piece.x + i, self.piece.y + j + 1)].occupied:
                    return True

        return False

    def place_piece(self):
        print('placing piece')
        for j, row in enumerate(self.piece.body):
            for i, cell in enumerate(row):
                if cell:
                    cur_field = self.board.fields[(self.piece.x + i, self.piece.y + j)]
                    cur_field.occupied = True
                    cur_field.color = self.piece.color

    def move_piece_safe(self, x, y) -> bool:
        if x < 0 or x + self.piece.width > cfg.x_count or y < 0 or y + self.piece.height > cfg.y_count:
            return False

        for j, row in enumerate(self.piece.body):
            for i, cell in enumerate(row):
                if cell and self.board.fields[(x + i, y + j)].occupied:
                    return False

        self.piece.x = x
        self.piece.y = y
        return True

    def move_piece_rel(self, delta_x, delta_y):
        x = self.piece.x + delta_x
        y = self.piece.y + delta_y
        self.move_piece_safe(x, y)

    def clear_lines(self):
        lines_to_remove = []
        for j in range(cfg.y_count - 1, 0, -1):
            all_row_occupied = True
            for i in range(cfg.x_count):
                if not self.board.fields[(i, j)].occupied:
                    all_row_occupied = False
                    break

            if all_row_occupied:
                lines_to_remove.append(j)

        if lines_to_remove:
            self.remove_specific_lines(lines_to_remove)

    def remove_specific_lines(self, lines: List[int]):
        print(f'removing lines {lines}')
        for line in lines:
            for x in range(cfg.x_count):
                self.board.fields[(x, line)] = Field()

        for line in reversed(lines):
            for y in range(line, 0, -1):
                for x in range(cfg.x_count):
                    current = self.board.fields.get((x, y))
                    if current and current.occupied:
                        self.board.fields[(x, y + 1)] = current
                        del self.board.fields[(x, y)]
                        self.board.fields[(x, y)] = Field()

    def get_ghost_piece_position(self) -> Tuple[int, int]:
        x = self.piece.x
        y = self.piece.y

        while y < cfg.y_count - self.piece.height:
            for j, row in enumerate(self.piece.body):
                for i, cell in enumerate(row):
                    if cell and self.board.fields[(x + i, y + j + 1)].occupied:
                        return x, y

            y += 1

        return x, y

    def hard_drop(self):
        self.piece.x, self.piece.y = self.get_ghost_piece_position()


class Piece:
    def __init__(self, body, color, x=3, y=0):
        self.body = np.array(body)
        self.x = x
        self.y = y
        self.color = color

    def move_abs(self, x, y):
        self.x = x
        self.y = y

    def move_rel(self, x, y):
        self.x += x
        self.y += y

    @property
    def width(self) -> int:
        return self.body.shape[1]

    @property
    def height(self) -> int:
        return self.body.shape[0]

    def reached_bottom(self) -> bool:
        result = self.y >= cfg.y_count - self.height
        if result:
            print('reached bottom')
        return result

    def rotate_clockwise(self):
        print('rotating clockwise')
        self.body = np.rot90(self.body, 3)

    def rotate_counterclockwise(self):
        print('rotating counterclockwise')
        self.body = np.rot90(self.body)


class IPiece(Piece):
    def __init__(self):
        super().__init__(body=[[1, 1, 1, 1]], color='cyan', x=3)


class OPiece(Piece):
    def __init__(self):
        super().__init__(body=[[1, 1], [1, 1]], color='yellow', x=4)


class TPiece(Piece):
    def __init__(self):
        super().__init__(body=[[0, 1, 0], [1, 1, 1]], color='purple')


class SPiece(Piece):
    def __init__(self):
        super().__init__(body=[[0, 1, 1], [1, 1, 0]], color='green')


class ZPiece(Piece):
    def __init__(self):
        super().__init__(body=[[1, 1, 0], [0, 1, 1]], color='red')


class JPiece(Piece):
    def __init__(self):
        super().__init__(body=[[1, 0, 0], [1, 1, 1]], color='blue')


class LPiece(Piece):
    def __init__(self):
        super().__init__(body=[[0, 0, 1], [1, 1, 1]], color='orange')


ALL_PIECES = (IPiece, OPiece, TPiece, SPiece, ZPiece, JPiece, LPiece)
