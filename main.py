import os
import sys

import pygame as pg
import win32con
import win32gui

import config as cfg
from models import Game

GRAVITY_DOWN = pg.USEREVENT


class App:
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pg.init()
    pg.display.set_mode((cfg.width, cfg.height))
    pg.display.set_caption(cfg.window_title)

    hwnd = pg.display.get_wm_info()['window']
    x, _, _, _ = win32gui.GetWindowRect(hwnd)
    win32gui.SetWindowPos(hwnd, win32con.HWND_TOPMOST, x, 5, cfg.width, cfg.height, 0)

    def __init__(self):
        self.screen = pg.display.get_surface()
        self.clock = pg.time.Clock()
        self.font = pg.font.SysFont(cfg.font_type, cfg.font_size, bold=False)

        self.game = Game()

        pg.time.set_timer(GRAVITY_DOWN, 1000 // self.game.gravity)
        # noinspection PyArgumentList
        pg.key.set_repeat(170, 50)

        self.game.spawn_new_piece()
        self.game.start()

        b_width = cfg.x_count * cfg.field_size
        b_height = cfg.y_count * cfg.field_size
        self.bg_rect = pg.Rect(cfg.padding, (b_width, b_height))

    def main_loop(self):
        while True:
            self.event_loop()
            self.update_everything()
            self.draw_everything()
            pg.display.flip()
            self.clock.tick_busy_loop(cfg.framerate)
            self.show_fps()

    def event_loop(self):
        for event in pg.event.get():
            # Press ESC or ❌ to quit
            if event.type == pg.QUIT or (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
                pg.quit()
                sys.exit()

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_SPACE:
                    self.game.hard_drop()

                if event.key == pg.K_LEFT:
                    self.game.move_piece_rel(-1, 0)

                if event.key == pg.K_RIGHT:
                    self.game.move_piece_rel(1, 0)

                if event.key == pg.K_DOWN:
                    self.game.move_piece_rel(0, 1)

                if event.key == pg.K_x:
                    self.game.piece.rotate_clockwise()

                if event.key == pg.K_z:
                    self.game.piece.rotate_counterclockwise()

            if event.type == GRAVITY_DOWN:
                self.game.gravity_piece()

    def update_everything(self):
        self.update_piece()

    def draw_everything(self):
        self.screen.fill(cfg.background_color)
        self.draw_board()
        self.draw_piece()

    def show_fps(self):
        pg.display.set_caption(f'{cfg.window_title} | FPS: {round(self.clock.get_fps())}')

    def draw_board(self):
        pg.draw.rect(self.screen, pg.Color('grey'), self.bg_rect)
        for k, v in self.game.board.fields.items():
            x, y = convert_position_to_coords(k)
            rect = pg.Rect(x, y, cfg.field_size - 1, cfg.field_size - 1)
            pg.draw.rect(self.screen, pg.Color(v.color), rect)

        x_start, y_start = convert_position_to_coords((0, 3))
        x_end, y_end = convert_position_to_coords((10, 3))
        pg.draw.line(self.screen, pg.Color('green'), (x_start, y_start - 2), (x_end, y_end - 2), 2)

    def draw_piece(self):
        ghost_x, ghost_y = self.game.get_ghost_piece_position()

        for j, row in enumerate(self.game.piece.body):
            for i, cell in enumerate(row):
                if cell == 1:
                    # drawing norm piece
                    x, y = convert_position_to_coords((self.game.piece.x + i,
                                                       self.game.piece.y + j))
                    pg.draw.rect(self.screen, pg.Color(self.game.piece.color),
                                 pg.Rect(x, y, cfg.field_size - 1, cfg.field_size - 1))

                    # drawing shadow piece
                    x, y = convert_position_to_coords((ghost_x + i,
                                                       ghost_y + j))
                    pg.draw.rect(self.screen, pg.Color('gray40'),
                                 pg.Rect(x, y, cfg.field_size - 1, cfg.field_size - 1))

    def update_piece(self):
        if self.game.is_piece_stopped():
            self.game.place_piece()
            self.game.clear_lines()
            self.game.spawn_new_piece()


def convert_position_to_coords(position, center=False):
    x_pad, y_pad = cfg.padding
    s = cfg.field_size

    if position is not None:
        x, y = position
        x = x * s + x_pad
        y = y * s + y_pad

        if center:
            x += s // 2
            y += s // 2
        return x, y

    return 0, 0


if __name__ == '__main__':
    app = App()
    app.main_loop()
